package org.rozasdev.fps.week2.lecture1

/**
  * High order functions.
  *
  * @author github.com/rozasdev
  */
object App {

  /**
  * Performs the sum of integers between two integers by applying a given function.
  * @param f The sum function.
  * @param a The first integer.
  * @param b The second integer.
  * @return Zero if a > b, else the sum of the factorials of all integers between a and b.
  */
  def genericSum(f: Integer => Integer, a: Integer, b: Integer): Integer =
    if(a > b) 0 else f(a) + genericSum(f, a+1, b)

  /**
  * Gets the sum of integers between two integers.
  * @param a The first integer.
  * @param b The second integer.
  * @return Zero if a > b, else the sum of all integers between a and b.
  */
  def sumInts(a: Int, b: Int): Int = genericSum(x => x, a, b)

  /**
  * Gets the sum of the cubes of integers between two integers.
  * @param a The first integer.
  * @param b The second integer.
  * @return Zero if a > b, else the sum of the cubes of all integers between a and b.
  */
  def sumCubes(a: Int, b: Int): Int = genericSum(x => x * x * x, a, b)

  /**
  * Gets the sum of the factorials of integers between two integers.
  * @param a The first integer.
  * @param b The second integer.
  * @return Zero if a > b, else the sum of the factorials of all integers between a and b.
  */
  def sumFactorials(a: Int, b: Int): Int = genericSum(factorial, a, b)

  /**
  * Calculates the factorial of an integer.
  * @param a An integer.
  * @return The factorial of a.
  */
  def factorial(a: Integer): Integer = if(a == 0) 1 else factorial(a-1) * a

}
