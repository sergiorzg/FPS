package org.rozasdev.fps.week1.lecture6

/**
  * Blocks & lexical scope.
  *
  * @author github.com/rozasdev
*/
object App {

  /**
    * Main function.
    * @param args Array of arguments.
    */
  def main(args: Array[String]): Unit ={
    if (args.length != 1)
      println(sqrt(25.0))
    else
      println(sqrt(args(0).toDouble))
  }

  /**
    * Sqrt function
    * @param x The value
    * @return The approximated sqrt.
    */
  def sqrt(x: Double) = {

    /**
      * Newton sqrt function (recursive).
      * @param guess The current root guess.
      * @return The approximated value for sqrt of param x.
      */
    def sqrtIter(guess: Double): Double =
      if (isGoodEnough(guess)) guess
      else sqrtIter(improve(guess))

    /**
      * Checks if the current guess is a good approximation
      * @param guess The current root guess.
      * @return True if the guess matches the criteria, else false.
      */
    def isGoodEnough(guess: Double): Boolean =
      abs(guess * guess - x) / x < 0.001

    /**
      * Gets a new approximation for the current guess and value.
      * @param guess The current root guess.
      * @return The new approximated value
      */
    def improve(guess: Double): Double =
      (guess + x / guess) / 2

    // Execute
    sqrtIter(1.0)
  }

  /**
    * Absolute value.
    * @param x The value.
    * @return Its absolute value
    */
  def abs(x: Double): Double = if (x >= 0) x else -x
}
