package org.rozasdev.fps.week1.lecture5

/**
  * Example for previous lectures.
  *
  * @author github.com/rozasdev
  */
object App {

  /**
    * Main function.
    * @param args Array of arguments.
    */
  def main(args: Array[String]): Unit ={
    if (args.length != 1)
      println(sqrt(25.0))
    else
      println(sqrt(args(0).toDouble))
  }

  /**
    * Sqrt function
    * @param x The value
    * @return The approximated sqrt.
    */
  def sqrt(x: Double) = sqrtIter(1.0, x)

  /**
    * Newton sqrt function (recursive).
    * @param guess The current root guess.
    * @param x The value which sqrt needs to be approximated.
    * @return The approximated value for sqrt of param x.
    */
  def sqrtIter(guess: Double, x: Double): Double =
    if(isGoodEnough(guess, x)) guess
    else sqrtIter(improve(guess,x), x)

  /**
    * Checks if the current guess is a good approximation
    * @param guess The current root guess.
    * @param x The original value of which the sqrt is being approximated.
    * @return True if the guess matches the criteria, else false.
    */
  def isGoodEnough(guess: Double, x: Double): Boolean =
    abs(guess * guess - x) / x < 0.001

  /**
    * Gets a new approximation for the current guess and value.
    * @param guess The current root guess.
    * @param x The original value of which the sqrt is being approximated.
    * @return The new approximated value
    */
  def improve(guess: Double, x: Double): Double =
    (guess + x / guess) / 2

  /**
    * Absolute value.
    * @param x The value.
    * @return Its absolute value
    */
  def abs(x: Double): Double = if (x >= 0) x else -x
}
